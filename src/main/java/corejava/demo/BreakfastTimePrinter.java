package corejava.demo;

public class BreakfastTimePrinter implements Executable {

    @Override
    public void execute(Long arg) {
        System.out.println("Breakfast Time");
    }
}
