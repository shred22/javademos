package corejava.demo;

import java.util.function.Predicate;

public class CheckIsBreakfastTime extends ExecutablePredicate {

    private final java.util.function.Predicate<Long> predicate;

    public CheckIsBreakfastTime(Predicate<Long> p) {
        this.predicate =p;
    }

    @Override
    public boolean evaluate(Long arg) {
        return predicate.test(arg);
    }

}
