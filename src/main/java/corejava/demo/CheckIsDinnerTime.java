package corejava.demo;

import java.util.function.Predicate;

public class CheckIsDinnerTime extends ExecutablePredicate {


    private final java.util.function.Predicate<Long> predicate;

    public CheckIsDinnerTime(Predicate<Long> p) {
        this.predicate =p;
    }

    @Override
    public boolean evaluate(Long arg) {
        return predicate.test(arg);
    }
}
