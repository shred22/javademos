package corejava.demo;

import java.util.function.Predicate;

public class CheckIsLunchTime extends ExecutablePredicate {

    private final Predicate<Long> predicate;

    public CheckIsLunchTime(Predicate<Long> p) {
     this.predicate =p;
    }

    @Override
    public boolean evaluate(Long arg) {
        return predicate.test(arg);
    }


}
