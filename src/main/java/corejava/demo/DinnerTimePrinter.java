package corejava.demo;

public class DinnerTimePrinter implements Executable {

    @Override
    public void execute(Long arg) {
        System.out.println("Dinner Time");
    }
}
