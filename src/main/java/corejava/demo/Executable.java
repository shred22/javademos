package corejava.demo;

import java.util.function.Predicate;

public interface Executable {
    public void execute(Long hourOfTheDay);
}
