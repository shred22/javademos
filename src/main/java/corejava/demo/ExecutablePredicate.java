package corejava.demo;

public abstract class ExecutablePredicate implements Executable, Predicate {

    private Executable onTrue;
    private Executable onFalse;

    @Override
    public void execute(Long hourOfTheDay) {
        boolean evaluationResult = evaluate(hourOfTheDay);
        if (evaluationResult) {
            onTrue.execute(hourOfTheDay);
        } else {
            onFalse.execute(hourOfTheDay);
        }
    }

    public ExecutablePredicate onTrue(Executable executable) {
        this.onTrue = executable;
        return this;
    }

    public ExecutablePredicate onFalse(Executable executable) {
        this.onFalse = executable;
        return this;
    }


}
