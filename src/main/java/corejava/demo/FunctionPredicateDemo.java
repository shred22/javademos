package corejava.demo;

public class FunctionPredicateDemo {

    public static void main(String[] args) {

        CheckIsBreakfastTime checkIsBreakfastTime = new CheckIsBreakfastTime(l -> l < 11L);

        CheckIsLunchTime checkIsLunchTime = new CheckIsLunchTime(l -> (l > 11L && l < 14L));

        CheckIsDinnerTime checkIsDinnerTime = new CheckIsDinnerTime(l -> (l > 19L && l < 21L));


        checkIsBreakfastTime.onTrue(new BreakfastTimePrinter()).onFalse(checkIsLunchTime);

        checkIsLunchTime.onTrue(new LunchTimePrinter()).onFalse(checkIsDinnerTime);

        checkIsDinnerTime.onTrue(new DinnerTimePrinter()).onFalse(new DefaultPrinter());

        checkIsBreakfastTime.execute(20L);

    }

}
