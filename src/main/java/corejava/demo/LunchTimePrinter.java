package corejava.demo;

public class LunchTimePrinter implements Executable {

    @Override
    public void execute(Long args) {
        System.out.println("Lunch Time");
    }
}
