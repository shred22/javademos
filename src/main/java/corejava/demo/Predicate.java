package corejava.demo;

public interface Predicate {

    public boolean evaluate( Long arg);

}
